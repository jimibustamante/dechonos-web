'use strict'

###*
 # @ngdoc function
 # @name dechonosApp.controller:MainCtrl
 # @description
 # # MainCtrl
 # Controller of the dechonosApp
###
angular.module 'dechonosApp'
  .controller 'MainCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]

    $scope.slide_images = ["/images/header-bg.jpg", "images/foto-banda.jpg"]
