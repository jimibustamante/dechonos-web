'use strict'

###*
 # @ngdoc function
 # @name dechonosApp.controller:AboutCtrl
 # @description
 # # AboutCtrl
 # Controller of the dechonosApp
###
angular.module 'dechonosApp'
  .controller 'AboutCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
