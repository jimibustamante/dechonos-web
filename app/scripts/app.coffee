'use strict'

###*
 # @ngdoc overview
 # @name dechonosApp
 # @description
 # # dechonosApp
 #
 # Main module of the application.
###
app = angular
  .module 'dechonosApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'spotify',
    'angular-flexslider'
  ]
  .config ($routeProvider) ->
    $routeProvider
      .when '/',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
      .when '/about',
        templateUrl: 'views/about.html'
        controller: 'AboutCtrl'
      .otherwise
        redirectTo: '/'

